(function ($) {
    $.addEventListener('DOMContentLoaded', function () {
        const body = $.getElementsByTagName('body')[0];
        const canvas = $.createElement('canvas');
        const c = canvas.getContext('2d');
        const img = new Image();

        img.src = 'html5.png';
        img.addEventListener('load', function () {
            canvas.width = 300;
            canvas.height = 300;

            c.drawImage(img, 0, 0, canvas.width, canvas.height);

            body.appendChild(canvas);
        }, false);
    }, false);
}(document));
