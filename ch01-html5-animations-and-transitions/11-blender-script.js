(function ($) {
    $.addEventListener('DOMContentLoaded', function () {
        const body = $.getElementsByTagName('body')[0];
        const renderer = new THREE.WebGLRenderer({ alpha: true });
        let scene = new THREE.Scene();
        const width = 500;
        const height = 300;
        const angle = 45;
        const aspect = width / height;
        const near = 0.1;
        const far = 10000;
        let camera = new THREE.PerspectiveCamera(angle, aspect, near, far);
        const monkeyLoader = new THREE.ObjectLoader();

        monkeyLoader.load('monkey.json', function (res) {
            camera = res.children.find(child => child.name === 'Camera');
            scene = res;
        });

        scene.add(camera);

        camera.position.z = 300;

        renderer.setSize(width, height);

        body.appendChild(renderer.domElement);

        function render() {
            renderer.render(scene, camera);
        }

        function update() {
            render();

            window.requestAnimationFrame(update);
        }

        update();
    }, false);
}(document));
