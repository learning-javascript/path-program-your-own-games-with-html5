(function ($) {
    $.addEventListener('DOMContentLoaded', function () {
        const body = $.getElementsByTagName('body')[0];
        const obj = $.createElement('div');
        let x = 0;
        let y = 0;
        let vx = 4;

        obj.style.width = 100 + 'px';
        obj.style.height = 100 + 'px';
        obj.style.backgroundColor = '#F00';

        obj.style.position = 'absolute';

        body.appendChild(obj);

        function update() {
            if (x + 100 > $.body.clientWidth || x < 0) {
                vx = -vx;
            }
            x += vx;
            obj.style.left = x + 'px';
            obj.style.top = y + 'px';
            window.requestAnimationFrame(update);
        }

        update();
    }, false);
}(document));
