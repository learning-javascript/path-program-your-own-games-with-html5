(function ($) {
    $.addEventListener('DOMContentLoaded', function () {
        const canvas = Raphael(0, 0, 300, 300);
        const rect = canvas.rect(0, 0, 100, 100);
        const img = new Image();

        rect.attr('fill', '#F00');

        // order of adding elements is important
        // this circle will be render before (under) the image
        canvas.circle(100, 100, 100);

        img.src = 'html5.png';

        canvas.image(img.src, 100, 0, 200, 200);

        // this circle will be render after (over) the image
        canvas.circle(200, 200, 100);
    }, false);
}(document));
