(function ($) {
    $.addEventListener('DOMContentLoaded', function () {
        const body = $.getElementsByTagName('body')[0];
        const obj = $.createElement('div');

        obj.style.width = 100 + 'px';
        obj.style.height = 100 + 'px';
        obj.style.backgroundColor = '#F00';

        obj.style.position = 'absolute';
//        obj.style.top = 200 + 'px';
//        obj.style.left = 400 + 'px';
        obj.style.webkitTransform = 'translateX(' + 200 + 'px)';
        obj.style.mozTransform = 'translateX(' + 200 + 'px)';
        obj.style.msTransform = 'translateX(' + 200 + 'px)';
        obj.style.transform = 'translateX(' + 200 + 'px)';

        body.appendChild(obj);
    }, false);
}(document));
