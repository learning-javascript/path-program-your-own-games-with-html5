(function ($) {
    $.addEventListener('DOMContentLoaded', function () {
        const body = $.getElementsByTagName('body')[0];
        const renderer = new THREE.WebGLRenderer({ alpha: true });
        let scene = new THREE.Scene();
        const width = 500;
        const height = 300;
        const angle = 45;
        const aspect = width / height;
        const near = 0.1;
        const far = 10000;
        let camera = new THREE.PerspectiveCamera(angle, aspect, near, far);
        const loader = new THREE.JSONLoader(true);
        let mesh = null;
        const duration = 700;
        const keyframes = 15;
        const interpolation = duration / keyframes;
        let lastKeyframe = 0;
        let currentKeyframe = 0;
        const light = new THREE.DirectionalLight(0xffffff, 2);

        light.position.set(1, 1, 1).normalize();
        scene.add(light);

        loader.load('horse.js', function (g) {
          mesh = new THREE.Mesh(g, new THREE.MeshLambertMaterial({color : 0x606060, morphTargets: true}));
          mesh.scale.set(1, 1, 1);
          mesh.rotation.y = 90;
          scene.add(mesh);
        });

        scene.add(camera);

        camera.position.z = 500;

        renderer.setSize(width, height);

        body.appendChild(renderer.domElement);

        function render() {
            if (mesh) {
                const time = Date.now() % duration;
                let keyframe = Math.floor(time / interpolation);

                if (keyframe !== currentKeyframe) {
                    mesh.morphTargetInfluences[lastKeyframe] = 0;
                    mesh.morphTargetInfluences[currentKeyframe] = 1;
                    mesh.morphTargetInfluences[keyframe] = 0;

                    lastKeyframe = currentKeyframe;
                    currentKeyframe = keyframe;
                }
                mesh.morphTargetInfluences[keyframe] = (time % interpolation) / interpolation;
                mesh.morphTargetInfluences[lastKeyframe] = 1 - mesh.morphTargetInfluences[keyframe];
            }
            renderer.render(scene, camera);
        }

        function update() {
            render();

            window.requestAnimationFrame(update);
        }

        update();
    }, false);
}(document));
