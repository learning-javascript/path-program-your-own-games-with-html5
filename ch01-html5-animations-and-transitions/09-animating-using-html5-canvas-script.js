(function ($) {
    $.addEventListener('DOMContentLoaded', function () {
        const body = $.getElementsByTagName('body')[0];
        const canvas = $.createElement('canvas');
        const c = canvas.getContext('2d');
        let x1 = 0;
        let y1 = 0;
        let vx = 2;
        let x2 = 0;
        let y2 = 0;
        let angle = 0;
        const radius = (canvas.width / 2) - 20;

        canvas.width = 300;
        canvas.height = 300;

        body.appendChild(canvas);

        function update() {
            c.fillStyle = '#000';
            c.fillRect(0, 0, canvas.width, canvas.height);

            if (x1 + 20 > canvas.width || x1 < 0) {
                vx = -vx;
            }
            x1 += vx;

            c.fillStyle = '#F00';
            c.fillRect(x1, y1, 20, 20);

            angle += 0.02;
            x2 = ((canvas.width - 20) / 2) + Math.sin(angle) * radius;
            y2 = ((canvas.height - 20) / 2) + Math.cos(angle) * radius;

            c.fillStyle = '#8AF';
            c.fillRect(x2, y2, 20, 20);

            window.requestAnimationFrame(update);
        }

        update();
    }, false);
}(document));
