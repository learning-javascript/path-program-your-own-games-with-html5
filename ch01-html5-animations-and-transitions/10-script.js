(function ($) {
    $.addEventListener('DOMContentLoaded', function () {
        const body = $.getElementsByTagName('body')[0];
        const renderer = new THREE.WebGLRenderer({ alpha: true });
        const scene = new THREE.Scene();
        const width = 500;
        const height = 300;
        const angle = 45;
        const aspect = width / height;
        const near = 0.1;
        const far = 10000;
        const camera = new THREE.PerspectiveCamera(angle, aspect, near, far);
        const cube = new THREE.CubeGeometry(100, 100, 100);
        const mat = new THREE.MeshBasicMaterial(new THREE.MeshBasicMaterial({ color: 0x0000ff, wireframe: true }));
        const mesh = new THREE.Mesh(cube, mat);
        let vx = 2;

        scene.add(mesh);
        scene.add(camera);

        camera.position.z = 300;

        renderer.setSize(width, height);

        body.appendChild(renderer.domElement);

        function render() {
            renderer.render(scene, camera);
        }

        function update() {
            render();

            if ((mesh.position.x > (width / 2) - 100) || (mesh.position.x < ((width / 2 ) * -1) + 100)) {
                vx = -vx;
            }

            mesh.position.x += vx;
            mesh.rotation.x += 0.02;

            window.requestAnimationFrame(update);
        }

        update();
    }, false);
}(document));
